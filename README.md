
    Install below mentioned modules to start with react js.


        1. npm install -g browserify
        2. npm install --save babelify
        3. npm install --save watchify
        4. npm install --save bebel-preset-react
        5. npm install --save react
        6. npm install --save react-dom


    After installing make some components and build project using below script in package.json


          "scripts": {
            "start": "watchify src/main.jsx -v -t [ babelify --presets [ react ] ] -o public/js/gp_bundle.js",
            "test": "echo \"Error: no test specified\" && exit 1"
          }


    npm start  (It will build project )

