var React = require('react');

var ListItem = require('./ListItems.jsx');

var ingredients = [
  {
    "id":1,
    "text": "gugu"
  },
  {
    "id":2,
    "text": "bubu"
  },
  {
    "id":3,
    "text": "gagudeep"
  },
];

var List = React.createClass({
    render: function() {
        var listItems = ingredients.map(function(item) {
          return <ListItem key={item.id} ingredients={item.text} />;
        });

        return (<ul>{listItems}</ul>);
    }
});

module.exports = List;
